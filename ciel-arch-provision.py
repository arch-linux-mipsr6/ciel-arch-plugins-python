#!/usr/bin/env python
import argparse
import subprocess


def execute(command: str) -> None:
    '''
    Executes an external command with unbuffered output

    Parameters:
    command: the external command to be run
    '''
    subprocess.run(command, shell=True)


def provision(url: str) -> None:
    '''
    Provision the workspace by loading the given image and configuring Ciel

    Parameters:
    url: URL to the system image to be used
    '''
    execute(f'ciel load-os \'{url}\'')
    execute('ciel config -g')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='provision an Arch-based Ciel workspace')
    parser.add_argument(
        'url',
        nargs=1,
        help=
        'URL to an Arch tarball that will be used as the container baseline')
    args = parser.parse_args()

    provision(args.url[0])
