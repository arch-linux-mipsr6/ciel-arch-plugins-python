# [Ciel](https://github.com/AOSC-Dev/ciel-rs) Plugins for Arch Linux (Python Rewrite)

[Ciel](https://github.com/AOSC-Dev/ciel-rs) is a container manager and integrated packaging environment created and used by the [AOSC OS](https://aosc.io) team. This tool is written in Rust and builds on top of `systemd-nspawn` and `overlayfs` to provide ephemeral containers for building software packages in a [clean environment](https://wiki.archlinux.org/title/DeveloperWiki:Building_in_a_clean_chroot).

While Ciel is originally designed with AOSC OS in mind, it has a distribution-agnostic core logic (requiring only the guest distribution to be `systemd`-based) and a plugin architecture that allows developers working with other distributions to tailor Ciel to fit the required workflow. This repository contains additional plugins that aim to better integrate with Arch Linux tools (mainly `pacman` and `makepkg`) and partially automate the packaging workflow.

While we at Arch Linux MIPS r6 will be using Ciel in conjunction with `qemu-user` to run `mipsisa64r6el` containers under emulation (requiring a recent Linux kernel with generic `execfd` support and correct `binfmt` configurations), Ciel should work equally well with native containers and should offer the same benefits stated above.

This repository contains the Python rewrite, which aims to make the code cleaner and easier to maintain, and the [Bash version](https://gitlab.com/arch-linux-mipsr6/ciel-arch-plugins) will be deprecated.

# List of Plugins

All Arch-specific plugins will have the `arch` prefix to avoid conflicting with AOSC's official Ciel actions and plugins.

- `ciel-arch-makepkg`: thin wrapper around the [`makepkg`](https://wiki.archlinux.org/title/makepkg) program
- `ciel-arch-provision`: provision an Arch-based workspace (due to a design limit of Ciel itself, one still has to manually run `ciel init` before invoking this plugin)
- `ciel-arch-sync-image`: sync (upgrade) packages in the container baseline Arch image 
