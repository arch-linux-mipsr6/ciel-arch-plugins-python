#!/usr/bin/env python
from typing import List
import argparse
import subprocess


def execute(command: str) -> None:
    '''
    Executes an external command with unbuffered output

    Parameters:
    command: the external command to be run
    '''
    subprocess.run(command, shell=True)


def concat(flags: List[str]) -> str:
    '''
    Combine multiple command line flags into one 

    Parameters:
    flags: a list of individual flags

    Returns: combined flags, in a string
    '''
    result = ''
    for f in flags:
        result = result + f'{f} '
    return result.rstrip()


def sync_image(flags: str) -> None:
    '''
    Sync (update) the Arch image in the current workspace.

    Parameters:
    flags: extra flags to be passed to pacman
    '''
    print('Creating temporary instance arch-sync-image...')
    execute('ciel add arch-sync-image')

    print('Syncing packages...')
    execute(f'ciel shell -i arch-sync-image "pacman -Syyu {flags}"')

    print('Committing changes...')
    execute('ciel commit -i arch-sync-image')

    print('Cleaning up...')
    execute('ciel del arch-sync-image')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='update the Arch image in the current Ciel workspace')
    parser.add_argument(
        'flags',
        nargs=argparse.REMAINDER,
        help=
        'extra flags to be passed to pacman, must be escaped from Ciel using --'
    )
    args = parser.parse_args()

    sync_image(concat(args.flags))
